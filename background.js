/**
 * Created by Brandon.Agbalog on 5/17/2017.
 */


chrome.webRequest.onCompleted.addListener(function (details) {

    console.log("starting switch on " + details.url);
    switch (true) {
        case (details.url.indexOf("rest/synthetic/monitor/filterdata") > -1 ):
            console.log("Sending UPDATE_SCATTERPLOT to tab ID " + details.tabId);
            chrome.tabs.sendMessage(details.tabId, {
                UPDATE_SCATTERPLOT: true,
                tab: details.tabId,
                url: details.url
            });
            break;

        default:
            console.log("no matches for " + details.url);
            break;
    }
}, {urls: ["<all_urls>"]});